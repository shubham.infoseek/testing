-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2017 at 11:19 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teen_patti`
--

-- --------------------------------------------------------

--
-- Table structure for table `mp_admin_user`
--

CREATE TABLE `mp_admin_user` (
  `id` int(11) NOT NULL,
  `F_NAME` varchar(250) NOT NULL,
  `L_NAME` varchar(250) NOT NULL,
  `EMAIL` varchar(250) NOT NULL,
  `PASSWORD` varchar(250) NOT NULL,
  `USER_ROLE` enum('ADMIN','S_ADMIN','OTHERS') NOT NULL,
  `creation_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mp_admin_user`
--

INSERT INTO `mp_admin_user` (`id`, `F_NAME`, `L_NAME`, `EMAIL`, `PASSWORD`, `USER_ROLE`, `creation_date`) VALUES
(1, 'Admin', '', 'admin@admin.com', '80802ed665136a1d27474361f85b32aa8ddc505bfac518d347d17f07a685e54b', '', '2017-11-24 14:23:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mp_admin_user`
--
ALTER TABLE `mp_admin_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mp_admin_user`
--
ALTER TABLE `mp_admin_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
